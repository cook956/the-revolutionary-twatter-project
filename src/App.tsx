import * as React from 'react'
import { StateHandlerMap, compose, withStateHandlers } from 'recompose'
import { Twatted } from './components/TwattedForm'
import { TwattList } from './components/TwattList'
import { WithStyles, withStyles, } from '@material-ui/core'


interface TwattState {
	twatts: Twatts
}

interface TwattHandler extends StateHandlerMap<TwattState> {
	addNew: (value: string) => any
	addComment: (id: string, value: string) => any
}

type Styles = 'root' | '@global' | 'header'
type OuterProps = {}
type InnerProps = OuterProps & WithStyles<Styles> & TwattState & TwattHandler

export interface Twatt {
	text: string
	comment: string[]
	lyk: boolean
}

export interface Twatts {
	[index: string]: Twatt
}


const enhance = compose<InnerProps, OuterProps>(
	withStyles<Styles>(theme => ({
		root: {
			display: 'flex',
			height: '100%',
			alignContent: 'center',
			alignItems: 'center',
			justifyContent: 'center',
			flexDirection: 'column' as 'column',
			padding: '16px',
		},

		header: {
			padding: '16px',
			color: '#ffff',
		},

		'@global': {
			'html,body,#root': {
				height: '100%',
				backgroundColor: '#282c34',
			},
		}
	})),
	withStateHandlers<TwattState, TwattHandler, OuterProps>(
		{
			twatts: {
				'1': {
					text: 'I once did something on Twatter',
					comment: [],
					lyk: false
				},
				'2': {
					text: 'I did another thing',
					comment: [],
					lyk: false
				}
			}
		},


		{

			addNew: state => (value: string) => {
				const twatts = state.twatts
				const keys = Object.keys(twatts)
				const lastKey = keys[keys.length - 1]
				const nextKey = Number(lastKey) + 1

				return ({
					twatts: {
						...twatts,
						[nextKey]: {
							text: value,
							lyk: false,
							comment: []
						}
					}
				})
			},
			addComment: state => (id: string, value: string) => {
				const twatts = state.twatts
				const twatt = twatts[id]
				twatt.comment.push(value)
				console.log({id, value})
				return ({
					twatts: {
						...twatts,
						[id]: twatt
					}
				})
			}
		},
	)
)

export const App = enhance(({ classes, addNew, twatts, addComment }) => {

	
	return (
		<div className={classes.root}>
			<header className={classes.header}> The Revolutionary Twatter </header>
			<Twatted addNew={addNew} />
			<TwattList twattData={twatts} addComment={addComment} />
		</div>
	)
})


