import React from 'react'
import { Fab, Paper, TextField, WithStyles, withStyles } from '@material-ui/core'
import { StateHandlerMap, compose, withStateHandlers } from 'recompose'

interface OuterProps {
    addNew: (value: string) => any
}

interface TwattedState {
    twatt: string
}

interface TwattedHandlers extends StateHandlerMap<TwattedState> {
    updateTwatt: (twatt: string) => any
}

type Styles = 'textfield' | 'spacing'
type InnerProps = OuterProps & TwattedState & TwattedHandlers & WithStyles<Styles>


const enhance = compose<InnerProps, OuterProps>(
    withStateHandlers<TwattedState, TwattedHandlers, OuterProps>(
        {
            twatt: ''
        },
        {
            updateTwatt: state => (twatt: string) => ({ twatt })
        }
    ),
    withStyles<Styles>(theme => ({
        textfield: {
            backgroundColor: '',
            flex: 1,
            marginRight: 16
        },
        spacing:{
            width: '50%',
            justifyContent: 'spacebewtween',
            display: 'flex',
            padding: 16,
        }
    }))
)

export const Twatted = enhance(({ classes, twatt, updateTwatt, addNew, }) => {
   
    return (
        <Paper className={classes.spacing}>
            <TextField
                className={classes.textfield}
                label="This is where you type"
                value={twatt}
                onChange={event => updateTwatt(event.target.value)}
            />
            <Fab onClick={() => {
                if (twatt !== '') {
                addNew(twatt)
                updateTwatt('')
                }
            }}>
                Twatt-it!
            </Fab>
        </Paper>
    )
})


