import React from 'react'
import { compose } from 'recompose'
import { Card, Paper, WithStyles, withStyles, } from '@material-ui/core'
import { Twatts } from '../App'
import { TwattListItem } from './ListItem'

interface OuterProps {
    twattData: Twatts
    addComment: (id: string, value: string) => any
}

type Styles = 'paper'
type InnerProps = OuterProps & WithStyles<Styles>

const enhance = compose<InnerProps, OuterProps>(
    withStyles<Styles>(() => ({
        paper: {
            marginTop: 16,
            padding: 16,
            width: '50%'
        },
        card:{
            maxWidth: '400',
        },
        actions: {
            display: 'flex',
        },
        expand: {
            transform: 'rotate(0deg)',
            marginLeft: 'auto',
        },
        expandOpen: {
            transform: 'rotate(180deg)'
        }
    }))
)



export const TwattList = enhance(({ classes, twattData, addComment }) => {
	const sortedKeys = Object.keys(twattData).sort((twattA, twattB) => {
		return Number(twattB) - Number(twattA)
	})
	
    return (
        <Paper className={classes.paper}>
            <Card>
                {sortedKeys.map(twatt => <TwattListItem twatt={twattData[twatt]} addComment={addComment} twattId={twatt} />)}
            </Card>
        </Paper>
    )
})
