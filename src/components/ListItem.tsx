import React from 'react'
import { Card, CardContent, CardActions, Collapse, Fab, TextField,Typography, WithStyles, withStyles, } from '@material-ui/core'
import { StateHandlerMap, compose, withStateHandlers } from 'recompose'
import { Twatt } from '../App';

interface OuterProps {
    addComment: (id: string, value: string) => any
    twattId: string
    twatt: Twatt
}
interface ListItemState {
    lyk: boolean
    commentOpen: boolean
    commentText: string

}

interface ListItemHandler extends StateHandlerMap<ListItemState> {
    toggleLyk: () => any
    toggleComments: () => any
}

type Styles = 'lykd' | 'fab' | 'list' | 'expand' | 'expandOpen'
type InnerProps = OuterProps & WithStyles<Styles> & ListItemState & ListItemHandler

const enhance = compose<InnerProps, OuterProps>(
    withStateHandlers<ListItemState, ListItemHandler, OuterProps>(
        {
            lyk: true,
            commentOpen: false,
            commentText: ''
        },
        {
            toggleLyk: ({ lyk }) => () => ({ lyk: !lyk }),
            toggleComments: ({ commentOpen }) => () => ({ commentOpen: !commentOpen }),
            updateComment: () => (commentText: string) => ({ commentText })
        }
    ),
    withStyles<Styles>(theme => ({
        fab: {
            justifyContent: 'flex',
            backgroundColor: '#33ccff',
            color: '#ffff',
        },
        lykd: {
            backgroundColor: '#f2f2f2',
        },
        list: {
            justifyContent: 'space-between'
        },
        expand: {
            transform: 'rotate(0deg)',
            // marginLeft: 'auto',
            backgroundColor: '#33ccff',
            transition: theme.transitions.create('transform', {
                duration: theme.transitions.duration.shortest,
            }),
        },
        expandOpen: {
            transform: 'rotate(360deg)',
            backgroundColor: '#f2f2f2',
        },
    })
    )
)

export const TwattListItem = enhance(({
    classes, lyk, toggleLyk, toggleComments, commentOpen, commentText,
    updateComment, addComment, twattId, twatt
}) => {
    const fabClass = lyk ? classes.lykd : classes.fab
    const commentClass = commentOpen ? classes.expand : classes.expandOpen

    return (
        <>
            <CardContent className={classes.list} >
                {twatt.text}
            </CardContent>
            {
                twatt.comment.map(comment => 
                <Card> 
                    <Typography> 
                        {comment} 
                    </Typography> 
                </Card> )
            }
            <CardActions>
                <Fab className={fabClass} onClick={() => toggleLyk()}>
                    {lyk ? 'Lyk-It' : 'Lyked'}
                </Fab>
                <Fab className={commentClass} onClick={() => toggleComments()}>
                    {commentOpen ? 'Said-IT' : 'Say-it'}
                </Fab>
            </CardActions>
            <Collapse in={commentOpen} >
                <CardContent>
                    <TextField
                        label="This is where you type"
                        value={commentText}
                        onChange={event => updateComment(event.target.value)}
                    />
                    <Fab onClick={() => {
                        if (commentText !== '') {
                            addComment(twattId, commentText)
                            updateComment('')
                        }
                    }}>
                        Save-it
            </Fab>
                </CardContent>
            </Collapse>
        </>
    )
})
